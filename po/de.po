# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Hendrik Knackstedt <kn.hendrik@gmail.com>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-05-26 07:07+0900\n"
"PO-Revision-Date: 2012-01-02 11:53+0200\n"
"Last-Translator: Hendrik Knackstedt <kn.hendrik@gmail.com>\n"
"Language-Team: Ubuntu German Translators\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Virtaal 0.7.0\n"

#: data/00_default.conf:4
#, sh-format
msgid "use $IM_CONFIG_DEFAULT_MODE mode (missing $IM_CONFIG_DEFAULT )"
msgstr "$IM_CONFIG_DEFAULT_MODE-Modus verwenden (fehlende $IM_CONFIG_DEFAULT )"

#: data/00_default.conf:7
#, sh-format
msgid "use $IM_CONFIG_DEFAULT_MODE mode (bogus content in $IM_CONFIG_DEFAULT)"
msgstr ""
"$IM_CONFIG_DEFAULT_MODE-Modus verwenden (falscher Inhalt in "
"$IM_CONFIG_DEFAULT)"

#: data/00_default.conf:9
#, sh-format
msgid "use $IM_CONFIG_DEFAULT_MODE mode set by $IM_CONFIG_DEFAULT"
msgstr "$IM_CONFIG_DEFAULT_MODE-Modus festgelegt durch $IM_CONFIG_DEFAULT"

#: data/01_auto.conf:2
#, fuzzy
msgid "activate IM with @-mark in its description"
msgstr "Eingabemethode mit @-Markierung für alle Sprachen aktivieren"

#: data/02_cjkv.conf:2
msgid "use $(mode_cjkv) mode based on locale and desktop"
msgstr ""

#: data/09_REMOVE.conf:2
#, sh-format
msgid "remove IM $IM_CONFIG_XINPUTRC_TYPE $IM_CONFIG_XINPUTRC"
msgstr "Eingabemethode $IM_CONFIG_XINPUTRC_TYPE $IM_CONFIG_XINPUTRC entfernen"

#: data/21_ibus.conf:2
msgid "activate Intelligent Input Bus (IBus)"
msgstr "Intelligenten Eingabebus (IBus) aktivieren"

#: data/21_ibus.conf:4
#, fuzzy
msgid ""
"Intelligent Input Bus (IBus)\n"
" * Required for all: ibus\n"
" * Language specific input conversion support:\n"
"   * Japanese: ibus-mozc (best) or ibus-anthy or ibus-skk\n"
"   * Korean: ibus-hangul\n"
"   * Simplified Chinese: ibus-pinyin or ibus-sunpinyin or ibus-libpinyin\n"
"   * Traditional Chinese: ibus-chewing or ibus-libzhuyin\n"
"   * Thai: ibus-thai or ibus-table-thai\n"
"   * Vietnamese: ibus-unikey or ibus-table-viqr\n"
"   * X Keyboard emulation: (internal)\n"
"   * Generic keyboard translation table: ibus-keyman, ibus-m17n, or ibus-"
"table*\n"
" * Application platform support:\n"
"   * Wayland: ibus-wayland\n"
"   * GNOME/GTK: ibus-gtk4, ibus-gtk3 and ibus-gtk\n"
"   * KDE/Qt: libqt5gui5\n"
"   * Clutter: ibus-clutter\n"
"   * EMACS: ibus-el"
msgstr ""
"Intelligenter Eingabebus (IBus)\n"
" * Immer erforderlich: ibus\n"
" * Sprachspezifische Unterstützung der Eingabemethode:\n"
"   * Japanisch: ibus-mozc (empfohlen) oder ibus-anthy oder ibus-skk\n"
"   * Koreanisch: ibus-hangul\n"
"   * Vereinfachtes Chinesisch: ibus-pinyin oder ibus-sunpinyin oder ibus-"
"googlepinyin\n"
"   * Traditionelles Chinesisch: ibus-chewing\n"
"   * Thailändisch: ibus-table-thai\n"
"   * Vietnamesisch: ibus-unikey oder ibus-table-viqr\n"
"   * X-Tastaturemulation: ibus-xkbc\n"
"   * Generische Tastaturübersetzungstabelle: ibus-m17n oder ibus-table*-"
"Pakete\n"
" * Anwendungsplattformunterstützung:\n"
"   * GNOME/GTK: ibus-gtk und ibus-gtk3 (beide)\n"
"   * KDE/Qt: ibus-qt4\n"
"   * Clutter: ibus-clutter\n"
"   * EMACS: ibus-el"

#: data/22_fcitx.conf:2
msgid "activate Flexible Input Method Framework (fcitx)"
msgstr "Flexibles Eingabemethodenrahmenwerk (fcitx) aktivieren"

#: data/22_fcitx.conf:4
#, fuzzy
msgid ""
"Flexible Input Method Framework (fcitx)\n"
" * Required for all: fcitx\n"
" * Language specific input conversion support:\n"
"   * Simplified Chinese: fcitx-libpinyin or fcitx-sunpinyin or fcitx-"
"googlepinyin\n"
"   * Traditional Chinese / generic Chinese: fcitx-rime\n"
"   * Generic keyboard translation table: fcitx-table* packages\n"
" * Application platform support:\n"
"   Installing fcitx-frontend-all will cover all GUI platforms.\n"
"   * GNOME/GTK: fcitx-frontend-gtk2 and fcitx-frontend-gtk3 (both)\n"
"   * KDE/Qt5: fcitx-frontend-qt5"
msgstr ""
"Flexibles Eingabemethodenrahmenwerk (fcitx)\n"
" * Immer erforderlich: fcitx\n"
" * Sprachspezifische Unterstützung der Eingabemethode:\n"
"   * Vereinfachtes Chinesisch: fcitx-pinyin oder fcitx-sunpinyin oder fcitx-"
"googlepinyin\n"
"   * Generische Tastaturübersetzungstabelle: fcitx-table*-Pakete\n"
" * Anwendungsplattformunterstützung:\n"
"   * GNOME/GTK: fcitx-frontend-gtk2 und fcitx-frontend-gtk3 (beide)\n"
"   * KDE/Qt4: fcitx-frontend-qt4"

#: data/23_fcitx5.conf:2
#, fuzzy
msgid "activate Flexible Input Method Framework v5 (fcitx5)"
msgstr "Flexibles Eingabemethodenrahmenwerk (fcitx) aktivieren"

#: data/23_fcitx5.conf:4
#, fuzzy
msgid ""
"Flexible Input Method Framework v5 (fcitx5)\n"
" * Required for all: fcitx5\n"
" * Language specific input conversion support:\n"
"   * Simplified Chinese: fcitx5-chinese-addons\n"
"   * Traditional Chinese / generic Chinese: fcitx5-rime\n"
"   * Generic keyboard translation table: fcitx5-keyman or fcitx5-table\n"
" * Application platform support:\n"
"   Installing recommended packages of fcitx5 will cover all GUI platforms.\n"
"   * GNOME/GTK: fcitx5-frontend-gtk3 and fcitx5-frontend-gtk4\n"
"   * KDE/Qt5: fcitx5-frontend-qt5 and kde-config-fcitx5"
msgstr ""
"Flexibles Eingabemethodenrahmenwerk (fcitx)\n"
" * Immer erforderlich: fcitx\n"
" * Sprachspezifische Unterstützung der Eingabemethode:\n"
"   * Vereinfachtes Chinesisch: fcitx-pinyin oder fcitx-sunpinyin oder fcitx-"
"googlepinyin\n"
"   * Generische Tastaturübersetzungstabelle: fcitx-table*-Pakete\n"
" * Anwendungsplattformunterstützung:\n"
"   * GNOME/GTK: fcitx-frontend-gtk2 und fcitx-frontend-gtk3 (beide)\n"
"   * KDE/Qt4: fcitx-frontend-qt4"

#: data/24_uim.conf:2
msgid "activate universal input method (uim)"
msgstr "Universelle Eingabemethode (uim) aktivieren"

#: data/24_uim.conf:4
#, fuzzy
msgid ""
"universal input method (uim)\n"
" * Required for all: uim\n"
" * Language specific input conversion support:\n"
"   * Japanese: uim-mozc (best) or uim-anthy or uim-skk\n"
"   * Korean: uim-byeoru\n"
"   * Simplified Chinese: uim-pinyin\n"
"   * Traditional Chinese: uim-chewing\n"
"   * Vietnamese: uim-viqr\n"
"   * General-purpose M17n: uim-m17nlib\n"
" * Application platform support:\n"
"   * XIM: uim-xim\n"
"   * GNOME/GTK: uim-gtk2.0 and uim-gtk3 (both)\n"
"   * KDE/Qt5: uim-qt5"
msgstr ""
"Universelle Eingabemethode (uim)\n"
" * Immer erforderlich: uim\n"
" * Sprachspezifische Unterstützung der Eingabemethode:\n"
"   * Japanisch: uim-mozc (empfohlen) oder uim-anthy oder uim-skk\n"
"   * Koreanisch: uim-byeoru\n"
"   * Vereinfachtes Chinesisch: uim-pinyin\n"
"   * Traditionelles Chinesisch: uim-chewing\n"
"   * Vietnamesisch: uim-viqr\n"
"   * Allgemein M17n: uim-m17nlib\n"
" * Anwendungsplattformunterstützung:\n"
"   * XIM: uim-xim\n"
"   * GNOME/GTK: uim-gtk2.0 und uim-gtk3 (beide)\n"
"   * KDE/Qt4: uim-qt\n"
"   * EMACS: uim-el"

#: data/25_hime.conf:2
#, fuzzy
msgid "activate HIME Input Method Editor (hime)"
msgstr "Flexibles Eingabemethodenrahmenwerk (fcitx) aktivieren"

#: data/25_hime.conf:4
#, fuzzy
msgid ""
"HIME Input Method Editor (hime)\n"
" * Required for all: hime\n"
" * Language specific input conversion support:\n"
"  * Traditional Chinese: hime-chewing\n"
"  * Japanese: hime-anthy\n"
" * Application platform support:\n"
"  * GNOME/GTK: hime-gtk2-immodule, hime-gtk3-immodule\n"
"  * KDE/Qt: hime-qt5-immodule"
msgstr ""
"Flexibles Eingabemethodenrahmenwerk (fcitx)\n"
" * Immer erforderlich: fcitx\n"
" * Sprachspezifische Unterstützung der Eingabemethode:\n"
"   * Vereinfachtes Chinesisch: fcitx-pinyin oder fcitx-sunpinyin oder fcitx-"
"googlepinyin\n"
"   * Generische Tastaturübersetzungstabelle: fcitx-table*-Pakete\n"
" * Anwendungsplattformunterstützung:\n"
"   * GNOME/GTK: fcitx-frontend-gtk2 und fcitx-frontend-gtk3 (beide)\n"
"   * KDE/Qt4: fcitx-frontend-qt4"

#: data/26_gcin.conf:2
msgid "activate Chinese input method (gcin)"
msgstr "Chinesische Eingabemethode (gcin) aktivieren"

#: data/26_gcin.conf:4
#, fuzzy
msgid ""
"Chinese input method (gcin)\n"
" * Required for all: gcin\n"
" * Language specific input conversion support:\n"
"  * Japanese: gcin-anthy\n"
" * Application platform support:\n"
"  * GNOME/GTK: gcin-gtk2-immodule, gcin-gtk3-immodule\n"
"  * KDE/Qt: gcin-qt5-immodule"
msgstr ""
"Flexibles Eingabemethodenrahmenwerk (fcitx)\n"
" * Immer erforderlich: fcitx\n"
" * Sprachspezifische Unterstützung der Eingabemethode:\n"
"   * Vereinfachtes Chinesisch: fcitx-pinyin oder fcitx-sunpinyin oder fcitx-"
"googlepinyin\n"
"   * Generische Tastaturübersetzungstabelle: fcitx-table*-Pakete\n"
" * Anwendungsplattformunterstützung:\n"
"   * GNOME/GTK: fcitx-frontend-gtk2 und fcitx-frontend-gtk3 (beide)\n"
"   * KDE/Qt4: fcitx-frontend-qt4"

#: data/30_maliit.conf:2
#, fuzzy
msgid "activate Mallit input method framework"
msgstr "Flexibles Eingabemethodenrahmenwerk (fcitx) aktivieren"

#: data/30_maliit.conf:4
msgid ""
"Mallit input method framework\n"
" * Required for everything: maliit-framework\n"
" * Keyboards part of (maliit-plugins):\n"
"   * reference keyboard: maliit-keyboard\n"
"   * QML keyboard: nemo-keyboard\n"
" * Application platform support:\n"
"   * GTK2: maliit-inputcontext-gtk2\n"
"   * GTK3: maliit-inputcontext-gtk3"
msgstr ""

#: data/48_scim.conf:2
msgid "activate Smart Common Input Method (SCIM)"
msgstr "Smart Common-Eingabemethode (SCIM) aktivieren "

#: data/48_scim.conf:4
#, fuzzy
msgid ""
"Smart Common Input Method (SCIM)\n"
" * Required for all: scim\n"
" * Language specific input conversion support:\n"
"   * Japanese: scim-mozc (best) or scim-anthy or scim-skk\n"
"   * Korean: scim-hangul\n"
"   * Simplified Chinese: scim-pinyin or scim-sunpinyin\n"
"   * Traditional Chinese: scim-chewing\n"
"   * Thai: scim-thai\n"
"   * Vietnamese: scim-unikey\n"
"   * Generic keyboard translation table: scim-m17 or scim-table* packages\n"
" * Application platform support:\n"
"   * GNOME/GTK: scim-gtk-immodule\n"
"   * Clutter: scim-clutter-immodule"
msgstr ""
"Smart Common-Eingabemethode (SCIM)\n"
" * Immer erforderlich: scim\n"
" * Sprachspezifische Unterstützung der Eingabemethode:\n"
"   * Japanisch: scim-mozc (empfohlen) oder scim-anthy oder scim-skk\n"
"   * Koreanisch: scim-hangul\n"
"   * Vereinfachtes Chinesisch: scim-pinyin oder scim-sunpinyin\n"
"   * Traditionelles Chinesisch: scim-chewing\n"
"   * Thailändisch: scim-thai\n"
"   * Vietnamesisch: scim-unikey\n"
"   * Generische Tastaturübersetzungstabelle: scim-m17 oder scim-table*-"
"Pakete\n"
" * Anwendungsplattformunterstützung:\n"
"   * GNOME/GTK: scim-gtk-immodule\n"
"   * KDE/Qt4: scim-qt-immodule\n"
"   * Clutter: scim-clutter-immodule"

#: data/60_thai.conf:2
msgid "activate Thai input method with thai-libthai"
msgstr "Thai-Eingabemethode mit thai-libthai aktivieren"

#: data/60_thai.conf:4
#, fuzzy
msgid ""
"Thai input method with thai-libthai\n"
" * GNOME/GTK: gtk-im-libthai and gtk3-im-libthai\n"
" * No XIM nor KDE/Qt support (FIXME)"
msgstr ""
"Thai-Eingabemethode mit thai-libthai\n"
" * GNOME/GTK: gtk-im-libthai und gtk3-im-libthai\n"
" * Keine Unterstützung für XIM oder KDE/Qt4 (FIXME)"

#: data/78_none.conf:2
#, fuzzy
msgid "do not activate any IM from im-config and use desktop default"
msgstr "Keinen Eingabemethode für die Eingabe verwenden"

#: data/78_none.conf:4
#, fuzzy
msgid ""
"This does not activate any IM from im-config.\n"
"This is the automatic configuration choice if no required IM packages are "
"installed, or if it is preferred to use desktop default."
msgstr ""
"Durch diese Einstellung wird keine Eingabemethode zur Eingabe verwendet.\n"
"Diese Einstellung wird von der automatischen Konfiguration verwendet, falls "
"keins der erforderlichen Eingabemethodenpakete installiert ist."

#: data/79_xim.conf:2
msgid "activate the bare XIM with the X Keyboard Extension"
msgstr ""

#: data/79_xim.conf:4
msgid ""
"This activates the bare XIM with the X Keyboard Extension for all softwares."
msgstr ""

#: data/80_kinput2.conf:2
msgid "activate XIM for Japanese with kinput2"
msgstr "XIM für Japanisch mit kinput2 aktivieren"

#: data/80_kinput2.conf:4
msgid ""
"X input method for Japanese with kinput2\n"
" * XIM: one of kinput2-* packages\n"
" * kanji conversion server: canna or wnn"
msgstr ""
"X-Eingabemethode für Japanisch mit kinput2\n"
" * XIM: eins der kinput2-*-Pakete\n"
" * kanji conversion server: canna oder wnn"

#: data/80_xsunpinyin.conf:2
msgid "activate XIM for Chinese with Sunpinyin"
msgstr "XIM für Chinesisch mit Sunpinyin aktivieren"

#: data/80_xsunpinyin.conf:4
msgid ""
"X input method for Chinese with Sunpinyin\n"
" * XIM: xsunpinyin"
msgstr ""
"X-Eingabemethode für Chinesisch mit Sunpinyin\n"
" * XIM: xsunpinyin"

#: data/90_bogus.conf:2
msgid "Bogus Configuration"
msgstr "Falsche Konfiguration"

#: data/90_bogus.conf:4
msgid "Non existing configuration name is specified."
msgstr "Kein existierender Konfigurationsname wurde angegeben."

#: data/90_custom.conf:2
msgid "Custom Configuration"
msgstr "Benutzerdefinierte Konfiguration"

#: data/90_custom.conf:4
msgid ""
"Custom configuration is created by the user or administrator using editor."
msgstr ""
"Eine benutzerdefinierte Konfiguration wird vom Benutzer oder Systemverwalter "
"mit einem Editor erstellt."

#: data/90_missing.conf:2
msgid "Missing"
msgstr "Fehlend"

#: data/90_missing.conf:4
msgid "Missing configuration file."
msgstr "Konfigurationsdatei fehlt."

#: im-config:15
msgid "system configuration"
msgstr "Systemkonfiguration"

#: im-config:18
msgid "user configuration"
msgstr "Benutzerkonfiguration"

#: im-config:28
msgid ""
"Explicit selection is not required to enable the automatic configuration if "
"the active one is default/auto/cjkv/missing."
msgstr ""
"Explizite Auswahl ist nicht erforderlich, um die automatische Konfiguration "
"zu aktivieren, falls default/auto/cjkv/missing aktiviert ist."

#: im-config:29
msgid ""
"If a daemon program for the previous configuration is re-started by the X "
"session manager, you may need to kill it manually with kill(1)."
msgstr ""
"Falls ein Dienstprogramm (Daemon) für die vorherige Konfiguration von der X-"
"Sitzungsverwaltung neu gestartet wird, müssen Sie es mit kill(1) evtl. "
"manuell beenden."

#: im-config:30
#, sh-format
msgid ""
"$IM_CONFIG_RTFM\n"
"See im-config(8) and /usr/share/doc/im-config/README.Debian.gz for more."
msgstr ""
"$IM_CONFIG_RTFM\n"
"Weitere Informationen unter im-config(8) und /usr/share/doc/im-config/README."
"Debian.gz."

#: im-config:47
#, sh-format
msgid "Input Method Configuration (im-config, ver. $IM_CONFIG_VERSION)"
msgstr "Konfiguration der Eingabemethode (im-config, ver. $IM_CONFIG_VERSION)"

#: im-config:94
#, sh-format
msgid ""
"$IM_CONFIG_ID\n"
"(c) Osamu Aoki <osamu@debian.org>, GPL-2+\n"
"See im-config(8), /usr/share/doc/im-config/README.Debian.gz."
msgstr ""
"$IM_CONFIG_ID\n"
"(c) Osamu Aoki <osamu@debian.org>, GPL-2+\n"
"Weitere Informationen unter im-config(8), /usr/share/doc/im-config/README."
"Debian."

#: im-config:137
#, sh-format
msgid "E: zenity must be installed."
msgstr ""

#: im-config:142
#, sh-format
msgid "E: X server must be available."
msgstr ""

#: im-config:148
#, sh-format
msgid "E: whiptail must be installed."
msgstr ""

#: im-config:157
#, sh-format
msgid ""
"The $IM_CONFIG_XINPUTRC_TYPE has been manually modified.\n"
"Remove the $IM_CONFIG_XINPUTRC_TYPE $IM_CONFIG_XINPUTRC manually to use im-"
"config.\n"
"$IM_CONFIG_RTFM"
msgstr ""
"$IM_CONFIG_XINPUTRC_TYPE wurde manuell bearbeitet.\n"
"Entfernen Sie den $IM_CONFIG_XINPUTRC_TYPE $IM_CONFIG_XINPUTRC manuell, um "
"im-config zu verwenden.\n"
"$IM_CONFIG_RTFM"

#: im-config:165
#, fuzzy, sh-format
msgid ""
"Current configuration for the input method:\n"
" * Default mode defined in /etc/default/im-config: "
"'$IM_CONFIG_DEFAULT_MODE'\n"
" * Active configuration: '$IM_CONFIG_ACTIVE' (normally missing)\n"
" * Normal automatic choice: '$IM_CONFIG_AUTOBASE' (normally ibus or fcitx5)\n"
" * Override rule: '$IM_CONFIG_PREFERRED_RULE'\n"
" * Current override choice: "
"'$IM_CONFIG_PREFERRED' (Locale='$IM_CONFIG_LC_CTYPE')\n"
" * Current automatic choice: '$IM_CONFIG_AUTOMATIC'\n"
" * Number of valid choices: $IM_CONFIG_NUMBER (normally 1)\n"
" * Desktop environment: '$IM_CONFIG_CURRENT_DESKTOP'\n"
"The configuration set by im-config is activated by re-starting the system."
msgstr ""
"Aktuelle Konfiguration der Eingabemethode:\n"
" * Aktive Konfiguration: $IM_CONFIG_ACTIVE (normalerweise missing)\n"
" * Automatische Konfiguration: $IM_CONFIG_AUTOMATIC (normalerweise ibus oder "
"fcitx oder uim)\n"
" * Anzahl gültiger Wahlen: $IM_CONFIG_NUMBER (normalerweise 1)\n"
"Die Konfiguration, die von im-config festgelegt wurde, wird nach einem "
"Neustart von X aktiviert."

#: im-config:178
#, fuzzy, sh-format
msgid ""
"$IM_CONFIG_MSG\n"
"In order to enter non-ASCII native characters, you must install one set of "
"input method tools:\n"
" * ibus and its associated packages (recommended)\n"
"   * multilingual support\n"
"   * GUI configuration\n"
" * fcitx5 and its associated packages\n"
"   * multilingual support with focus on Chinese\n"
"   * GUI configuration\n"
" * uim and its associated packages\n"
"   * multilingual support\n"
"   * manual configuration with the Scheme code\n"
"   * text terminal support even under non-X environments\n"
" * any set of packages which depend on im-config\n"
"$IM_CONFIG_MSGA"
msgstr ""
"$IM_CONFIG_MSG\n"
"Um nicht-ASCII-Zeichen einzugeben, müssen Sie einen Satz an Werkzeugen für "
"eine Eingabemethode installieren:\n"
" * ibus und die zugehörigen Pakete (empfohlen)\n"
"   * Mehrsprachenunterstützung\n"
"   * Konfiguration über grafische Oberfläche\n"
" * fcitx und die zugehörigen Pakete\n"
"   * Mehrsprachenunterstützung mit Fokus auf Chinesisch\n"
"   * Konfiguration über grafische Oberfläche\n"
" * uim und die zugehörigen Pakete\n"
"   * Mehrsprachenunterstützung\n"
"   * Manuelle Konfiguration mit Schemen-Code\n"
"   * Text-Terminal-Unterstützung selbst in nicht-X-Umgebungen\n"
" * irgendeine Paketsammlung, die von im-config abhängt\n"
"$IM_CONFIG_MSGA"

#: im-config:195
#, sh-format
msgid ""
"$IM_CONFIG_MSG\n"
"$IM_CONFIG_MSGA"
msgstr ""
"$IM_CONFIG_MSG\n"
"$IM_CONFIG_MSGA"

#: im-config:199
#, sh-format
msgid ""
"$IM_CONFIG_MSG\n"
"$IM_CONFIG_MSGA\n"
"  Available input methods:$IM_CONFIG_AVAIL\n"
"Unless you really need them all, please make sure to install only one input "
"method tool."
msgstr ""
"$IM_CONFIG_MSG\n"
"$IM_CONFIG_MSGA\n"
"  Verfügbare Eingabemethoden:$IM_CONFIG_AVAIL\n"
"Wenn Sie nicht wirklich alle Eingabemethoden benötigen, stellen Sie sicher, "
"dass Sie nur das benötigte Eingabemethodenwerkzeug installieren."

#: im-config:207
#, sh-format
msgid ""
"Do you explicitly select the ${IM_CONFIG_XINPUTRC_TYPE}?\n"
"\n"
" * Select NO, if you do not wish to update it. (recommended)\n"
" * Select YES, if you wish to update it."
msgstr ""
"Wählen Sie ${IM_CONFIG_XINPUTRC_TYPE} explizit aus?\n"
"\n"
" * Wählen Sie NEIN, falls Sie es nicht aktualisieren möchten. (empfohlen)\n"
" * Wählen Sie JA, falls Sie es aktualisieren möchten."

#: im-config:215
#, fuzzy, sh-format
msgid ""
"Select $IM_CONFIG_XINPUTRC_TYPE. The user configuration supersedes the "
"system one."
msgstr ""
"Wählen Sie $IM_CONFIG_XINPUTRC_TYPE. Die Benutzerkonfiguration überschreibt "
"die Systemkonfiguration."

#: im-config:216
msgid "select"
msgstr "Auswahl"

#: im-config:217
msgid "name"
msgstr "Name"

#: im-config:218
msgid "description"
msgstr "Beschreibung"

#: im-config:246
#, sh-format
msgid "Removing the $IM_CONFIG_XINPUTRC_TYPE $IM_CONFIG_XINPUTRC."
msgstr "$IM_CONFIG_XINPUTRC_TYPE $IM_CONFIG_XINPUTRC wird entfernt."

#: im-config:247 im-config:266
#, fuzzy, sh-format
msgid ""
"\n"
"The $IM_CONFIG_XINPUTRC_TYPE is modified by im-config.\n"
"\n"
"Restart the session to activate the new $IM_CONFIG_XINPUTRC_TYPE.\n"
"$IM_CONFIG_RTFM"
msgstr ""
"\n"
"$IM_CONFIG_XINPUTRC_TYPE wird von im-config bearbeitet.\n"
"\n"
"Starten Sie die X-Sitzung neu, um die neue $IM_CONFIG_XINPUTRC_TYPE zu "
"aktivieren.\n"
"$IM_CONFIG_RTFM"

#: im-config:255 im-config:282
#, sh-format
msgid "Keeping the $IM_CONFIG_XINPUTRC_TYPE $IM_CONFIG_XINPUTRC as missing."
msgstr ""
"$IM_CONFIG_XINPUTRC_TYPE $IM_CONFIG_XINPUTRC wird als »fehlend« behalten."

#: im-config:257 im-config:284
#, sh-format
msgid ""
"Keeping the $IM_CONFIG_XINPUTRC_TYPE $IM_CONFIG_XINPUTRC unchanged as "
"$IM_CONFIG_ACTIVE."
msgstr ""
"$IM_CONFIG_XINPUTRC_TYPE $IM_CONFIG_XINPUTRC wird unverändert als "
"$IM_CONFIG_ACTIVE behalten."

#: im-config:265
#, sh-format
msgid ""
"Setting the $IM_CONFIG_XINPUTRC_TYPE $IM_CONFIG_XINPUTRC to "
"$IM_CONFIG_ACTIVE."
msgstr ""
"$IM_CONFIG_XINPUTRC_TYPE $IM_CONFIG_XINPUTRC wird als $IM_CONFIG_ACTIVE "
"festgelegt."

#: im-config:274
#, sh-format
msgid ""
"*** This is merely a simulated run and no changes are made. ***\n"
"\n"
"$IM_CONFIG_MSG"
msgstr ""
"*** Dies ist nur eine Simulation und es werden keine Änderungen "
"durchgeführt. ***\n"
"\n"
"$IM_CONFIG_MSG"

#: im-config:293
#, sh-format
msgid ""
"$IM_CONFIG_MSG\n"
"Automatic configuration selects: $IM_CONFIG_AUTOMATIC\n"
"$IM_CONFIG_AUTOMATIC_LONG\n"
"$IM_CONFIG_RTFM"
msgstr ""
"$IM_CONFIG_MSG\n"
"Automatische Konfigurationsauswahl: $IM_CONFIG_AUTOMATIC\n"
"$IM_CONFIG_AUTOMATIC_LONG\n"
"$IM_CONFIG_RTFM"

#: im-config:300
#, sh-format
msgid ""
"$IM_CONFIG_MSG\n"
"Manual configuration selects: $IM_CONFIG_ACTIVE\n"
"$IM_CONFIG_ACTIVE_LONG\n"
"$IM_CONFIG_RTFM"
msgstr ""
"$IM_CONFIG_MSG\n"
"Manuelle Konfigurationsauswahl: $IM_CONFIG_ACTIVE\n"
"$IM_CONFIG_ACTIVE_LONG\n"
"$IM_CONFIG_RTFM"

#: share/im-config.common:51 share/im-config.common:69
#: share/im-config.common:87 share/xinputrc.common.in:90
#, sh-format
msgid "E: Configuration for $IM_CONFIG_NAME not found at $IM_CONFIG_CODE."
msgstr ""
"E: Konfiguration für $IM_CONFIG_NAME unter $IM_CONFIG_CODE nicht gefunden."

#: share/im-config.common:116
#, sh-format
msgid ""
"E: Configuration in $IM_CONFIG_XINPUTRC is manually managed. Doing nothing."
msgstr ""

#: share/im-config.common:120
#, sh-format
msgid ""
"E: $IM_CONFIG_NAME is bogus configuration for $IM_CONFIG_XINPUTRC. Doing "
"nothing."
msgstr ""

#: share/xinputrc.common.in:44
#, fuzzy, sh-format
msgid "I: Script for $IM_CONFIG_NAME started at $IM_CONFIG_CODE."
msgstr "Skript für $IM_CONFIG_NAME unter $IM_CONFIG_CODE gestartet."

#: share/xinputrc.common.in:49
#, sh-format
msgid "E: Script for $IM_CONFIG_NAME not found at $IM_CONFIG_CODE."
msgstr "E: Skript für $IM_CONFIG_NAME unter $IM_CONFIG_CODE nicht gefunden."

#: im-config.desktop.in:3
msgid "Input Method"
msgstr "Eingabemethode"

#: im-config.desktop.in:4
msgid "Set Keyboard Input Method"
msgstr ""

#: im-config.desktop.in:6
msgid "keyboard;input"
msgstr ""

#, fuzzy
#~ msgid "use auto mode only under CJKV"
#~ msgstr "Eingabemethode mit @-Markierung nur für CJKV aktivieren"

#~ msgid "activate Hangul (Korean) input method"
#~ msgstr "Hangul-Eingabemethode (Koreanisch) aktivieren"

#~ msgid ""
#~ "Hangul (Korean) input method\n"
#~ " * XIM: nabi\n"
#~ " * GNOME/GTK: imhangul-gtk2 and imhangul-gtk3\n"
#~ " * KDE/Qt4: qimhangul-qt4\n"
#~ " * GUI companion: imhangul-status-applet"
#~ msgstr ""
#~ "Hangul-Eingabemethode (Koreanisch)\n"
#~ " * XIM: nabi\n"
#~ " * GNOME/GTK: imhangul-gtk2 und imhangul-gtk3\n"
#~ " * KDE/Qt4: qimhangul-qt4\n"
#~ " * GUI companion: imhangul-status-applet"

#~ msgid ""
#~ "The $IM_CONFIG_XINPUTRC_TYPE is modified by im-config.\n"
#~ "Restart the X session to activate the new $IM_CONFIG_XINPUTRC_TYPE.\n"
#~ "$IM_CONFIG_RTFM"
#~ msgstr ""
#~ "$IM_CONFIG_XINPUTRC_TYPE wird von im-config bearbeitet.\n"
#~ "Starten Sie die X-Sitzung neu, um die neue $IM_CONFIG_XINPUTRC_TYPE zu "
#~ "aktivieren.\n"
#~ "$IM_CONFIG_RTFM"

#~ msgid ""
#~ "Chinese input method (gcin)\n"
#~ " * XIM and GNOME/GTK: gcin\n"
#~ " * KDE/Qt4: gcin-qt4-immodule"
#~ msgstr ""
#~ "Chinesische Eingabemethode (gcin)\n"
#~ " * XIM und GNOME/GTK: gcin\n"
#~ " * KDE/Qt4: gcin-qt4-immodule"
